import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
void main() {
  runApp(DonArgent());
}
final List<String> imgList = [
  'http://amalyy.com/image/imagesCas/Demande%20de%20denr%C3%A9es%20alimentaires/1618064774.jpeg',
  'http://amalyy.com/image/imagesCas/Demande%20de%20denr%C3%A9es%20alimentaires/1618064774.jpg',
  'http://amalyy.com/image/imagesCas/Demande%20de%20denr%C3%A9es%20alimentaires/1618064774.jpg',
  'http://amalyy.com/image/imagesCas/Demande%20de%20denr%C3%A9es%20alimentaires/1618064774.jpg',
];

final List<Widget> imageSliders = imgList.map((item) => Container(
  child: Container(
    margin: EdgeInsets.all(5.0),
    child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        child: Stack(
          children: <Widget>[
            Image.network(item, fit: BoxFit.cover, width: 1000.0),
            Positioned(
              bottom: 0.0,
              left: 0.0,
              right: 0.0,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(200, 0, 0, 0),
                      Color.fromARGB(0, 0, 0, 0)
                    ],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                  ),
                ),
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                /*child: Text(
                  'No. ${imgList.indexOf(item)} image',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),*/
              ),
            ),
          ],
        )
    ),
  ),
)).toList();

class DonArgent extends StatefulWidget {
  DonArgent({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _DonArgentState createState() => _DonArgentState();
}

class _DonArgentState extends State<DonArgent> {
  int _current = 0; String strPhoneArgent, strMontant;
  String strOperateur;
  @override
  Widget build(BuildContext context) {

    final phoneArgent = TextFormField(
      keyboardType: TextInputType.number,
      autofocus: false,
      initialValue: '',
      //validator: validatePhone,
      onSaved: (String val) {
        strPhoneArgent = val;
      },
      decoration: InputDecoration(
        hintText: 'Votre numéro pour le retrait',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(2.0)),
      ),
    );

    final montant = TextFormField(
      keyboardType: TextInputType.number,
      autofocus: false,
      initialValue: '',
      //validator: validatePhone,
      onSaved: (String val) {
        strMontant = val;
      },
      decoration: InputDecoration(
        hintText: 'Montant',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(2.0)),
      ),
    );

    final operateur =  new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            new Text('Orange Money'),
            new Radio(
                value: 'Orange',
                groupValue: strOperateur,
                onChanged: (value)
                {
                  setState(() {
                    strOperateur = value;
                  });
                }
            ),
          ],
        ),
        new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            new Text('Mobile Money'),
            new Radio(
                value: 'MTN',
                groupValue: strOperateur,
                onChanged: (value)
                {
                  setState(() {
                    strOperateur = value;
                  });
                }
            ),
          ],
        ),

      ],
    );

    return  Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: [
          SizedBox(height: 10),
          Column(
            children: [
              CarouselSlider(
                items: imageSliders,
                options: CarouselOptions(
                    autoPlay: true,
                    enlargeCenterPage: true,
                    aspectRatio: 2.0,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _current = index;
                      });
                    }
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: imgList.map((url) {
                  int index = imgList.indexOf(url);
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index
                          ? Color.fromRGBO(0, 0, 0, 0.9)
                          : Color.fromRGBO(0, 0, 0, 0.4),
                    ),
                  );
                }).toList(),
              ),
            ],
          ),
          SizedBox(height: 10),
          Center(
            child: Column(

              children: [
                Text(
                    'Titre du cas',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold
                    )
                ),
                SizedBox(height: 20),
                Padding(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    child:Column(
                      children: [
                        montant,
                        SizedBox(height: 20.0),
                        /*phoneArgent,
                        SizedBox(height: 20.0),*/
                        operateur,
                        SizedBox(
                            width: 300,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(elevation:15, onSurface: Colors.blue),
                              onPressed: () {

                              },

                              child: Text('Valider LE DonArgent'),
                            )
                        ),
                      ],
                    )
                )

              ],
            ),
          ),


        ],
      ),
    );
  }

}




