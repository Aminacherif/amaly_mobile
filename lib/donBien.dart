import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'dart:async';

import 'package:multi_image_picker/multi_image_picker.dart';
void main() {
  runApp(DonBien());
}
final List<String> imgList = [
  'http://amalyy.com/image/imagesCas/Demande%20de%20denr%C3%A9es%20alimentaires/1618064774.jpeg',
  'http://amalyy.com/image/imagesCas/Demande%20de%20denr%C3%A9es%20alimentaires/1618064774.jpg',
  'http://amalyy.com/image/imagesCas/Demande%20de%20denr%C3%A9es%20alimentaires/1618064774.jpg',
  'http://amalyy.com/image/imagesCas/Demande%20de%20denr%C3%A9es%20alimentaires/1618064774.jpg',

];

final List<Widget> imageSliders = imgList.map((item) => Container(
  child: Container(
    margin: EdgeInsets.all(5.0),
    child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        child: Stack(
          children: <Widget>[
            Image.network(item, fit: BoxFit.cover, width: 1000.0),
            Positioned(
              bottom: 0.0,
              left: 0.0,
              right: 0.0,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(200, 0, 0, 0),
                      Color.fromARGB(0, 0, 0, 0)
                    ],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                  ),
                ),
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                /*child: Text(
                  'No. ${imgList.indexOf(item)} image',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),*/
              ),
            ),
          ],
        )
    ),
  ),
)).toList();

class DonBien extends StatefulWidget {
  DonBien({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _DonBienState createState() => _DonBienState();
}

class _DonBienState extends State<DonBien> {
  int _current = 0;
  String strNomBiens;

  List<Asset> images = List<Asset>();
  String _error = 'No Error Dectected';
  @override
  void initState() {
    super.initState();
  }

  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return AssetThumb(
          asset: asset,
          width: 300,
          height: 300,
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      _error = error;
    });
  }

  @override
  Widget build(BuildContext context) {

    String validateNomBiens(String value) {
      if (value.length == 0) {
        return "Le nom des biens est obligatoire";
      }
      return null;
    }
    final nomBiens = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      initialValue: '',
      validator: validateNomBiens,
      onSaved: (String val) {
        strNomBiens = val;
      },
      decoration: InputDecoration(
        hintText: 'Nom bien(s)',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(2.0)),
      ),
    );

    return  Scaffold(
      body: Column(
        //shrinkWrap: true,
        children: [
          SizedBox(height: 10),
          Column(
            children: [
              CarouselSlider(
                items: imageSliders,
                options: CarouselOptions(
                    autoPlay: true,
                    enlargeCenterPage: true,
                    aspectRatio: 2.0,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _current = index;
                      });
                    }
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: imgList.map((url) {
                  int index = imgList.indexOf(url);
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index
                          ? Color.fromRGBO(0, 0, 0, 0.9)
                          : Color.fromRGBO(0, 0, 0, 0.4),
                    ),
                  );
                }).toList(),
              ),
            ],
          ),
          SizedBox(height: 10),
          Center(
            child: Column(

              children: [
                Text(
                    'Demande de denrées alimentaires',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold
                    )
                ),
                SizedBox(height: 20),
                Padding(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    child:Column(
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: [
                              Text('Veuillez remplir les informations du don'),
                              nomBiens,


                            ],
                          ),
                        )


                      ],
                    )
                )

              ],
            ),
          ),
          RaisedButton(
            child: Text("Images"),
            onPressed: loadAssets,
          ),
          Expanded(
            child: buildGridView(),
          ),


        ],
      ),
    );
  }

}




