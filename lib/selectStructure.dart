import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:amaly/model/structure.dart';

Future <List<Structures>> fetchStructures() async {
  final response = await http.get('http://amalyy.com/api/users');
  if (response.statusCode == 200) {
    List jsonResponse = json.decode(response.body);
    return jsonResponse.map((structures) => new Structures.fromJson(structures)).toList();
  } else {
    throw Exception('Unexpected error occured!');
  }
}




void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {



    return MaterialApp(
      title: 'Faire un Don à',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: Structuress(title: 'Faire un Don à'),
    );
  }
}

class Structuress extends StatefulWidget {
  Structuress({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _StructuressState createState() => _StructuressState();
}

class _StructuressState extends State<Structuress> {

  Future <List<Structures>> futureStructures;
  @override
  void initState() {
    super.initState();
    futureStructures = fetchStructures();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Faire un Don à'),
        centerTitle: true,
      ),

      body: FutureBuilder <List<Structures>>(
        future: futureStructures,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Structures> structures = snapshot.data;
            return ListView.builder(
                itemCount: structures.length,
                itemBuilder:(context , index){
                  return new Container(
                    padding: EdgeInsets.all(5.0),
                    height: 100.0,
                    child : new Card(
                      elevation: 5.0,
                      child : new InkWell(
                        onTap: (()=> print('Tape')),
                        onLongPress: (()=> print('long')),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40,
                              height: 40,
                              margin: EdgeInsets.only(left: 15.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image:DecorationImage(
                                    image: NetworkImage('http://amalyy.com/image/imagesUsers/${structures[index].photo}'),
                                    fit: BoxFit.fill
                                ),
                              ),

                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.0),
                              child: Text(structures[index].compagnie,
                                style: new TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold
                                ),
                              ) ,
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }


            );

          } else if (snapshot.hasError) {
            return Text("Veuillez vérifier votre internet");
          }
          //
          // By default show a loading spinner.
          return Center(child: CircularProgressIndicator());
        },
      ),
    );




  }



}



























