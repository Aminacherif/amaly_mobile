class Don {
  final int id;
  final int cas_id;
  final String type;
  final String don;
  final String quartier;
  final String ville;
  final String description;
  final String telephone;
  final String image1;
  final String image2;
  final String image3;
  final String image4;
  final String commentaire;
  final int userphones_id;
  final String created_at;
  final String updated_at;

  Don({this.id, this.cas_id, this.type,this.don,this.quartier,this.ville,this.description,this.telephone,this.image1,this.image2,this.image3,this.image4,this.commentaire,this.userphones_id,this.created_at,this.updated_at});

  factory Don.fromJson(Map<String, dynamic> json) {
    return Don(
        id : json['id'],
        cas_id : json['cas_id'],
        type : json['type'],
        don : json['don'],
        quartier : json['quartier'],
        ville : json['ville'],
        description : json['description'],
        telephone : json['telephone'],
        image1 : json['image1'],
        image2 : json['image2'],
        image3 : json['image3'],
        image4 : json['image4'],
        commentaire : json['commentaire'],
        userphones_id : json['userphones_id'],
        created_at : json['created_at'],
        updated_at : json['updated_at']
    );
  }
}