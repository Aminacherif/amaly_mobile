class Structures {
  final int id;
  final String profil;
  final String nom;
  final String prenom;
  final String compagnie;
  final String poste;
  final String telephone;
  final String genre;
  final String deleted_at;
  final String photo;
  final String ville;
  final String quartier;
  final int statut;
  final String description;
  final String email;
  final String email_verified_at;
  final String created_at;
  final String updated_at;

  Structures({this.id, this.profil, this.nom,this.prenom,this.compagnie,this.poste,this.telephone,this.genre,this.deleted_at,this.photo,this.ville,this.quartier,this.statut,this.description,this.email,this.email_verified_at,this.created_at,this.updated_at});

  factory Structures.fromJson(Map<String, dynamic> json) {
    return Structures(
        id : json['id'],
        profil : json['profil'],
        nom : json['nom'],
        prenom : json['prenom'],
        compagnie : json['compagnie'],
        poste : json['poste'],
        telephone : json['telephone'],
        genre : json['genre'],
        deleted_at : json['deleted_at'],
        photo : json['photo'],
        ville : json['ville'],
        quartier : json['quartier'],
        statut : json['statut'],
        description : json['description'],
        created_at : json['created_at'],
        updated_at : json['updated_at']
    );
  }
}