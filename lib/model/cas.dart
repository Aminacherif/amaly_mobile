class Cas {
 final int id;
 final String titre;
 final int categories_id;
 final String description;
 final String image1;
 final String image2;
 final String image3;
 final String image4;
 final String numero;
 final String deleted_at;
 final int montant;
 final String ville;
 final String quartier;
 final int statut;
 final int users_id;
 final String created_at;
 final String updated_at;

  Cas({this.id, this.titre, this.categories_id,this.description,this.image1,this.image2,this.image3,this.image4,this.numero,this.deleted_at,this.montant,this.ville,this.quartier,this.statut,this.users_id,this.created_at,this.updated_at});

 factory Cas.fromJson(Map<String, dynamic> json) {
   return Cas(
    id : json['id'],
    titre : json['titre'],
    categories_id : json['categories_id'],
    description : json['description'],
    image1 : json['image1'],
    image2 : json['image2'],
    image3 : json['image3'],
    image4 : json['image4'],
    numero : json['numero'],
    deleted_at : json['deleted_at'],
    montant : json['montant'],
    ville : json['ville'],
    quartier : json['quartier'],
    statut : json['statut'],
    users_id : json['users_id'],
    created_at : json['created_at'],
    updated_at : json['updated_at']
   );
  }
}