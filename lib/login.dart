import 'dart:convert';
import 'dart:io';
import 'package:amaly/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Env.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'home.dart';
import 'register.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String strEmail, strPassword;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  SharedPreferences sharedPreferences;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ProgressDialog pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true);
    pr.style(
        message: "Veuillez patienter",
        progressWidget: CircularProgressIndicator());

    void performLogin() async {
      pr.show();

      final http.Response response =await http.post('http://amalyy.com/api/login', body: {
        "email": strEmail,
        "password": strPassword,
      });

      print("reponse ${response.body}");
      if (response.statusCode == 200) {
        print("\n\nsuccess");

        Map<String, dynamic> reponse = jsonDecode(response.body);
        if (reponse['errors']==true) {
          pr.hide();
          setState(() {
            final snackbar = new SnackBar(
              content: new Text(reponse['message']),
              backgroundColor: Colors.redAccent,
            );
            scaffoldKey.currentState.showSnackBar(snackbar);
          });
        } else {
          pr.hide();


          SharedPreferences localStorage = await SharedPreferences.getInstance();
          localStorage.setString('token', json.encode(reponse['token']));
          localStorage.setString('user', json.encode(reponse['user']));
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => new Home()));
        }
      } else {
        pr.hide().whenComplete(() {
          setState(() {
            final snackbar = new SnackBar(
              content: new Text("Connexion au serveur impossible !!"),
              backgroundColor: Colors.redAccent,
            );
            scaffoldKey.currentState.showSnackBar(snackbar);
          });
        });
      }

    }

    String validateLogin(String value) {
      if (value.length == 0) {
        return "Votre pseudo est obligatoire";
      }
      return null;
    }

    String validatePassword(String value) {
      if (value.length == 0) {
        return "Le mot de passe est obligatoire";
      }
      return null;
    }

    void _submit() {
      final form = formKey.currentState;

      if (form.validate()) {
        form.save();
        performLogin();


      }
    }

    final logo = Hero(
      tag: 'Amaly',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('images/amaly9.jpg'),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      initialValue: '',
      validator: validateLogin,
      onSaved: (String val) {
        strEmail = val;
      },
      decoration: InputDecoration(
        hintText: 'Identifiant',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      initialValue: '',
      obscureText: true,
      validator: validatePassword,
      onSaved: (String val) {
        strPassword = val;
      },
      decoration: InputDecoration(
        hintText: 'Mot de passe',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: _submit,
        padding: EdgeInsets.all(12),
        color: Colors.blue,
        child: Text('Connexion ', style: TextStyle(color: Colors.white)),
      ),
    );

    return Scaffold(
        backgroundColor: Colors.white,
        key: scaffoldKey,
        body: Center(
          child: Form(
            key: formKey,
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 10.0),
              children: <Widget>[
                logo,
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Veuillez renseigner vos identifiants",
                  style: TextStyle(color: Colors.blue, fontSize: 15.0),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 48.0),
                email,
                SizedBox(height: 8.0),
                password,
                SizedBox(height: 24.0),
                loginButton,
                SizedBox(height: 24.0),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => RegisterFirst()));
                  },
                  child: Text(
                    "S'inscrire",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.blue,
                      fontSize: 15.0,
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
