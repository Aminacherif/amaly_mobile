import 'package:flutter/material.dart';
class Nouvelle_page extends StatelessWidget
{
  String title;
  Nouvelle_page(String title)
  {
    this.title=title;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.blue,
          title: new Text(
            title,
            style: new TextStyle(
                color: Colors.white,
                fontStyle: FontStyle.italic
            ),
          ),
        ),
        body : new Center(
          child: new Text(
            'Je suis une nouvelle page',
            style: new TextStyle(
                color: Colors.blue,
                fontStyle: FontStyle.italic
            ),),

        )

    );
    throw UnimplementedError();
  }


}