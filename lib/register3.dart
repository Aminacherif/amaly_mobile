import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'Env.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'home.dart';

class RegisterThird extends StatefulWidget {
  String prenom,nom,email,phone,mdp,mdpcfm;

  RegisterThird(String prenom, String nom, String email, phone, mdp, mdpcfm){
    this.prenom = prenom;
    this.nom = nom;
    this.email = email;
    this.phone = phone;
    this.mdp = mdp;
    this.mdpcfm = mdpcfm;
  }

  @override
  _RegisterThirdState createState() => new _RegisterThirdState(prenom,nom,email,phone,mdp,mdpcfm);
}

class _RegisterThirdState extends State<RegisterThird> {
  String prenom,nom,email,phone,mdp,mdpcfm;
  bool v = true;

  _RegisterThirdState(String prenom, String nom, String email, phone, mdp, mdpcfm){
    this.prenom = prenom;
    this.nom = nom;
    this.email = email;
    this.phone = phone;
    this.mdp = mdp;
    this.mdpcfm = mdpcfm;
  }
  String strGenre;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  SharedPreferences sharedPreferences;


  @override
  Widget build(BuildContext context) {
    ProgressDialog pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true);
    pr.style(
        message: "Veuillez patienter",
        progressWidget: CircularProgressIndicator());

    void performSignin() async {
      pr.show();

      print(Env.SIGNIN_URL);

        final http.Response response = await http.post('http://amalyy.com/api/register', body: {
          "nom": nom,
          "prenom": prenom,
          "email": email,
          "telephone": phone,
          "password": mdp,
          "password_confirmation": mdpcfm,
          "genre": strGenre
        });

        print("reponse ${response.body}");
        if (response.statusCode == 200) {
          Map<String, dynamic> reponse = jsonDecode(response.body);
          if (reponse['error']==true) {
            pr.hide();
            setState(() {
              final snackbar = new SnackBar(
                content: new Text(reponse['message']),
                backgroundColor: Colors.redAccent,
              );
              scaffoldKey.currentState.showSnackBar(snackbar);
            });
          } else {
            pr.hide();
            sharedPreferences = await SharedPreferences.getInstance();
            sharedPreferences.setString("id", reponse['user']['id']);
            sharedPreferences.setString("nom", reponse['user']['nom']);
            sharedPreferences.setString("prenom", reponse['user']['prenom']);
            sharedPreferences.setString("email", reponse['user']['email']);
            sharedPreferences.setString("phone", reponse['user']['phone']);
            sharedPreferences.setString("genre", reponse['user']['genre']);
            sharedPreferences.setBool("connected", true);

          }
        } else {
          pr.hide().whenComplete(() {
            setState(() {
              final snackbar = new SnackBar(
                content: new Text("Connexion au serveur impossible !!"),
                backgroundColor: Colors.redAccent,
              );
              scaffoldKey.currentState.showSnackBar(snackbar);
            });
          });
        }

    }


    void _submit() {
      final form = formKey.currentState;
      form.save();
        performSignin();
      /*Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => new Home()));*/

    }

    final logo = Hero(
      tag: 'Media7plus',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('images/amaly9.jpg'),
      ),
    );

    final genre =  new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            new Text('Homme'),
            new Radio(
                value: 'Masculin',
                groupValue: strGenre,
                onChanged: (value)
                {   
                  setState(() {
                  strGenre = value;
                });
                }
            ),
          ],
        ),
        new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            new Text('Femme'),
            new Radio(
                value: 'Feminin',
                groupValue: strGenre,
                onChanged: (value)
                {
                  setState(() {
                    strGenre = value;
                  });
                }
            ),
          ],
        ),
        new Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            new Text('Autres'),
            new Radio(
                value: 'Autre',
                groupValue: strGenre,
                onChanged: (value)
                {
                  setState(() {
                    strGenre = value;
                  });
                }
            )
          ],
        )



      ],
    );
    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: _submit,
        padding: EdgeInsets.all(12),
        color: Colors.blue,
        child: Text("S'inscrire", style: TextStyle(color: Colors.white)),
      ),
    );


    return Scaffold(
        backgroundColor: Colors.white,
        key: scaffoldKey,
        body: Center(
          child: Form(
            key: formKey,
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 10.0),
              children: <Widget>[
                logo,
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Veuillez renseigner vos informations",
                  style: TextStyle(color: Colors.blue, fontSize: 15.0),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 48.0),
                genre,
                SizedBox(height: 18.0),
                (v)?Text('Votre genre est obligatoire',style: TextStyle(color: Colors.red,),textAlign: TextAlign.center,):Text(''),
                SizedBox(height: 48.0),
                loginButton,

              ],
            ),
          ),
        ));
  }
}
