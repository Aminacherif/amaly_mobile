import 'dart:convert';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:amaly/model/dons.dart';

Future <List<Don>> fetchDon() async {
  final response = await http.get('http://amalyy.com/api/donsPerso');
  if (response.statusCode == 200) {
    List jsonResponse = json.decode(response.body);
    return jsonResponse.map((don) => new Don.fromJson(don)).toList();
  } else {
    throw Exception('Unexpected error occured!');
  }
}




void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {



    return MaterialApp(
      title: 'Mes Dons',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: Dons(title: 'Mes Dons'),
    );
  }
}

class Dons extends StatefulWidget {
  Dons({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _DonsState createState() => _DonsState();
}

class _DonsState extends State<Dons> {
  
  Future <List<Don>> futureDon;
  @override
  void initState() {
    super.initState();
    futureDon = fetchDon();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mes Dons'),
        centerTitle: true,
      ),

      body: FutureBuilder <List<Don>>(
        future: futureDon,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Don> don = snapshot.data;

            return ListView.builder(
                itemCount: don.length,
                itemBuilder:(context , index){
                  return new Container(
                      padding: EdgeInsets.all(5.0),
                      height: MediaQuery.of(context).size.height/6,
                      child : new Card(
                        elevation: 5.0,
                          child: new Container(
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                      width:MediaQuery.of(context).size.width/3,
                                      height:MediaQuery.of(context).size.height/4,
                                      child:Image.network('http://amalyy.com/image/imagesDons/${don[index].image1}',fit: BoxFit.cover,)

                                  ),
                                  
                                  Container(

                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,

                                      children: [

                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Don : ',) ,
                                            Text(don[index].don)
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Type : ',) ,
                                            Text(don[index].type)
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Description : ',) ,
                                            Text(don[index].description)
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Cas : ',) ,
                                            Text(don[index].cas_id.toString())
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Commentaire : ',) ,
                                            Text(don[index].commentaire)
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Date : ',) ,
                                            Text(don[index].created_at)
                                          ],
                                        ),


                                      ],
                                    ),
                                  )

                                 


                                  // margin: EdgeInsets.only(left: 20.0),
                                ],
                              ),
                            )

                          ),
                        // color: setColor(),
                      )
                  );

                }


            );

          } else if (snapshot.hasError) {
            return Text("Veuillez vérifier votre internet");
          }
          // By default show a loading spinner.
          return Center(child: CircularProgressIndicator());
        },
      ),
    );

      

    
  }
  


}



























