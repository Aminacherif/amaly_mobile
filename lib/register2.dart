
import 'package:amaly/register3.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'register3.dart';

class RegisterSecond extends StatelessWidget {
  String prenom,nom,email;

  RegisterSecond(String prenom, String nom, String email){
    this.prenom = prenom;
    this.nom = nom;
    this.email = email;
  }
  String strPhone, strMdp, strMdpCfm,strMdpCfmSubmitted,strMdpSubmitted;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();


 /* @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }*/

  @override
  Widget build(BuildContext context) {
    ProgressDialog pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true);
    pr.style(
        message: "Veuillez patienter",
        progressWidget: CircularProgressIndicator());

    String validatePhone(String value) {
      if(value == null) {
        return null;
      }
      final n = num.tryParse(value);
      if(n == null) {
        return 'Veuillez saisir un numéro de téléphone';
      }
      return null;
    }


    String validateMdp(String value) {
      if (value.length == 0) {
        return "Votre mot de passe est obligatoire";
      }
      return null;
    }

    String validateMdpcfm(String value) {
      if (value.length == 0) {
        return "Veuillez confirmer votre mot de passe";
      }
      else if(strMdp!=strMdpCfm){
        return "Le mot de passe ne coorespond pas";
      }
      return null;
    }

   /* String comfirmeMdp(){
      if(strMdp==strMdpCfm){
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => new RegisterThird(prenom,nom,email,strPhone,strMdp,strMdpCfm)));
      }else{
        return "Le champ de confirmation mot de passe ne correspond pas";
      }
    }*/


    void _submit() {
      final form = formKey.currentState;
      if (form.validate()) {
        form.save();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => new RegisterThird(prenom,nom,email,strPhone,strMdp,strMdpCfm)));
       //comfirmeMdp();
      }

    }

    final logo = Hero(
      tag: 'Media7plus',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('images/amaly9.jpg'),
      ),
    );

    final phone = TextFormField(
      keyboardType: TextInputType.number,
      autofocus: false,
      inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
      initialValue: '1223',
      validator: validatePhone,
      onSaved: (String val) {
        strPhone = val;
      },

      decoration: InputDecoration(
        hintText: 'Telephone',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final mdp = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      obscureText: true,
      initialValue: 'zertyuio',
      validator: validateMdp,
      onChanged: (value) => strMdp = value,
      decoration: InputDecoration(
        hintText: 'Mot de passe',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final mdpCfm = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      obscureText: true,
      initialValue: 'sdfghjk',
      validator: validateMdpcfm,
      onChanged: (value) => strMdpCfm = value,
      decoration: InputDecoration(
        hintText: 'Confirmer le mot de passe',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: _submit,
        padding: EdgeInsets.all(12),
        color: Colors.blue,
        child: Text('Suivant ', style: TextStyle(color: Colors.white)),
      ),
    );

    return Scaffold(
        backgroundColor: Colors.white,
        key: scaffoldKey,
        body: Center(
          child: Form(
            key: formKey,
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 10.0),
              children: <Widget>[
                logo,
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Veuillez renseigner vos informations",
                  style: TextStyle(color: Colors.blue, fontSize: 15.0),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 48.0),
                phone,
                SizedBox(height: 8.0),
                mdp,
                SizedBox(height: 8.0),
                mdpCfm,
                SizedBox(height: 18.0),
                //Text(comfirmeMdp()),
                SizedBox(height: 24.0),
                loginButton,




              ],
            ),
          ),
        ));
  }
}
