import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'dart:async';

//import 'package:multi_image_picker/multi_image_picker.dart';



class PhotoProfil extends StatefulWidget {
  PhotoProfil({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PhotoProfilState createState() => _PhotoProfilState();
}

class _PhotoProfilState extends State<PhotoProfil> {



  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:ListView(
        shrinkWrap: true,
        children: [
          Stack(
            children: [
              Container(
                margin: EdgeInsets.only(left: 3.0, right: 3.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: Colors.blueGrey.withOpacity(0.3), spreadRadius: 2,)
                    ],

                  borderRadius: new BorderRadius.vertical(
                      bottom: new Radius.elliptical(
                          MediaQuery.of(context).size.width, 100.0)),


                ),
                child:Column(
                  children: [
                   Container(
                     margin: EdgeInsets.only(top: 30.0),
                     height: MediaQuery.of(context).size.height/5,
                     child:  Image.asset('images/amaly8.jpg',fit: BoxFit.cover),
                   ),
                   Container(
                      margin: const EdgeInsetsDirectional.only(start: 2, end: 2, bottom: 2),
                      height: MediaQuery.of(context).size.height/10,
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.vertical(
                            bottom: new Radius.elliptical(
                                MediaQuery.of(context).size.width, 100.0)),

                      ),

                    ),
                  ],
                )

              ),
              Container(
                alignment: Alignment.topCenter,
                padding: new EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * .250,
                    right: 20.0,
                    left: 20.0),
                child: Container(
                  child: SizedBox(
                      height: 120.0,
                      width: 120.0,
                    child: CircularProfileAvatar(
                      'http://amalyy.com/image/imagesUsers/CHERIF/1618151997.jpeg',
                      radius: 100,
                     // backgroundColor: Colors.transparent,
                      //borderWidth: 10,
                     // borderColor: Colors.brown,
                      elevation: 10.0,
                      //foregroundColor: Colors.brown.withOpacity(0.5),
                      cacheImage: false, // allow widget to cache image against provided url
                      onTap: () {
                        print('adil');
                      }, // sets on tap
                      showInitialTextAbovePicture: true, // setting it true will show initials text above profile picture, default false
                    ),
                   )

                ),
              )


            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height/40
          ),
          Center(
            child: Text('Amina Chérif',
                    style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.blueGrey
                )
            )
          ),
          SizedBox(height: MediaQuery.of(context).size.height/10),
          Center(
            child: Stack(
              children: [
                Container(
                  margin:EdgeInsets.only(left: 130.0,top: 30.0),
                  child: Icon(Icons.favorite_border,size: 80,color: Colors.blue,),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  margin: new EdgeInsets.only(
                      top: MediaQuery.of(context).size.height /200,
                      right: 00.0,
                      left: 220.0),
                  child: Icon(Icons.favorite_border,size: 50,color: Colors.blue,),
                )
              ]
            )
          ),
          SizedBox(height: MediaQuery.of(context).size.height/10),
          Center(
              child: Text('Faites un don pour sauver une vie',
                  style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.blueGrey
                  )
              )
          ),
          SizedBox(height: 35.0,),
          Container(
            padding: EdgeInsets.only(left: 10.0,right: 10.0),
            child:  RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
              onPressed: null,
              padding: EdgeInsets.all(12),
              color: Colors.blue,
              child: Text('Faire un Don ', style: TextStyle(fontSize:15.0,color: Colors.white)),
            ),
          )


        ],
      )


    );
  }
  }




