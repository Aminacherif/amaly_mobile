import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Env.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'home.dart';
import 'register2.dart';
import 'package:email_validator/email_validator.dart';


class RegisterFirst extends StatefulWidget {

  @override
  _RegisterFirstState createState() => new _RegisterFirstState();
}

class _RegisterFirstState extends State<RegisterFirst> {
  String strLastName, strFirstName, strEmail;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ProgressDialog pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true);
    pr.style(
        message: "Veuillez patienter",
        progressWidget: CircularProgressIndicator());

    String validateFirstName(String value) {
      if (value.length == 0) {
        return "Votre prénom est obligatoire";
      }
      return null;
    }

    String validateLastName(String value) {
      if (value.length == 0) {
        return "Votre nom est obligatoire";
      }
      return null;
    }


    void _submit() {
      final form = formKey.currentState;

      if (form.validate()) {
        form.save();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => new RegisterSecond(strFirstName ,strLastName,strEmail)));
      }


    }

    final logo = Hero(
      tag: 'Media7plus',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('images/amaly9.jpg'),
      ),
    );

    final firstName = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      initialValue: 'ikol',
      validator: validateFirstName,
      onSaved: (String val) {
        strFirstName = val;
      },
      decoration: InputDecoration(
        hintText: 'Prenom',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final lastName = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      initialValue: 'dcfvgbhjk',
      validator: validateLastName,
      onSaved: (String val) {
        strLastName = val;
      },
      decoration: InputDecoration(
        hintText: 'Nom',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      initialValue: 'zertyu@gmail.com',
      onSaved: (String val) {
        strEmail = val;
      },
      validator: (strEmail) => EmailValidator.validate(strEmail) ? null : "Veuillez entrer une adresse email valide",
      autovalidate: true,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: _submit,
        padding: EdgeInsets.all(12),
        color: Colors.blue,
        child: Text('Suivant ', style: TextStyle(color: Colors.white)),
      ),
    );

    return Scaffold(
        backgroundColor: Colors.white,
        key: scaffoldKey,
        body: Center(
          child: Form(
            key: formKey,
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 10.0),
              children: <Widget>[
                logo,
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Veuillez renseigner vos informations",
                  style: TextStyle(color: Colors.blue, fontSize: 15.0),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 48.0),
                firstName,
                SizedBox(height: 8.0),
                lastName,
                SizedBox(height: 8.0),
                email,
                SizedBox(height: 24.0),
                loginButton,


              ],
            ),
          ),
        ));
  }
}
