import 'dart:convert';
import 'package:amaly/network_utils/dons.dart';
import 'package:amaly/selectStructure.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:http/http.dart' as http;
import 'model/cas.dart';
import 'donArgent.dart';
import 'donBien.dart';
import'broullon.dart';
import 'photoProfil.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'login.dart';
import 'model/structure.dart';
import 'package:url_launcher/url_launcher.dart';
import 'network_utils/api.dart';

Future <List<Cas>> fetchCas() async {
  SharedPreferences localStorage = await SharedPreferences.getInstance();
  var token = localStorage.getString('token');
  final response =
  await http.get('http://amalyy.com/api/cas');
  if (response.statusCode == 200) {
    List jsonResponse = json.decode(response.body);
    return jsonResponse.map((cas) => new Cas.fromJson(cas)).toList();
  } else {
    throw Exception('Unexpected error occured!');
  }
}

Future <List<Structures>> fetchStructures() async {
  SharedPreferences localStorage = await SharedPreferences.getInstance();
  var token = localStorage.getString('token');
  final response = await http.get('http://amalyy.com/api/users');
  if (response.statusCode == 200) {
    List jsonResponse = json.decode(response.body);
    return jsonResponse.map((structures) => new Structures.fromJson(structures)).toList();
  } else {
    throw Exception('Unexpected error occured!');
  }
}




void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {



    return MaterialApp(
      title: 'Amaly',
      theme: ThemeData(
       // visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: Home(title: 'Amaly'),
    );
  }
}

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String strTypeDon;
  bool notificationCouleur = true;
  int _selectedDestination = 0;

  void _launchLink() async {
    const url = "http://amalyy.com/login";

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw "Impossible de lancer le lien.";
    }
  }


  void selectDestination(int index) {
    setState(() {
      _selectedDestination = index;
    });
  }


  Future <List<Cas>> futureCas;
  Future <List<Structures>> futureStructures;
  @override
  void initState() {
  super.initState();
  futureCas = fetchCas();
  futureStructures = fetchStructures();
  }




  @override
  Widget build(BuildContext context) {

    Future<Null>  logout() async{
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context){
            return new AlertDialog(
              title: new Text("Alerte"),
              content: new Text("Etes vous sûr de vouloir vous déconnecter  ?") ,
              actions: <Widget>[
                new FlatButton(
                  onPressed: (){
                    Navigator.pop(context);
                  },
                  child: new Text("Non"),
                  textColor: Colors.red,
                ),
                new FlatButton(
                  onPressed: () async {
                      var res = await Network().getData('/signout');
                      var body = json.decode(res.body);
                      if(body['success']){
                        SharedPreferences localStorage = await SharedPreferences.getInstance();
                        localStorage.remove('user');
                        localStorage.remove('token');
                        Navigator.pushReplacement(context,
                            new MaterialPageRoute(builder: (context) => LoginPage()));
                      }



                  },
                  child: new Text("Oui"),
                  textColor: Colors.green,
                ),
              ],
            );
          }
      );
    }

    print(notificationCouleur);
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.home)),
              Tab(icon: Icon(Icons.notifications)),
              Tab(icon: Icon(Icons.format_align_justify)),
            ],
          ),
          title: Text('Amaly'),
        ),
        drawer:  Drawer(

          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      height: 80.0,
                      width: 80.0,
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image:DecorationImage(
                              image: NetworkImage('http://amalyy.com/image/imagesUsers/CHERIF/1618151997.jpeg'),
                              fit: BoxFit.fill
                          ),
                        ),
                      ),


                    ),
                    Text('Amina cherif',
                      style: new TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          decorationColor: Colors.white,
                          color:Colors.white
                      ),
                    ) ,
                  ],
                ),

                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'Actions',
                ),
              ),
              ListTile(
                  leading: Icon(Icons.favorite),
                  title: Text('Mes Dons'),
                  selected: _selectedDestination == 0,
                  onTap: (){
                    selectDestination(0);
                    Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context )
                    {
                      return new Dons();
                    }));
                  }
              ),
              ListTile(
                  leading: Icon(Icons.tag_faces),
                  title: Text('Faire un Don (ONG/Orphelinat)'),
                  selected: _selectedDestination == 1,
                  onTap: (){
                    selectDestination(1);
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => new Structuress()));
                  }
              ),
              ListTile(
                leading: Icon(Icons.label),
                title: Text('Créer un Cas'),
                selected: _selectedDestination == 2,
                onTap: (){
                  selectDestination(2);
                  _launchLink();
                }
              ),
              Divider(
                height: 1,
                thickness: 1,
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'Paramètres',
                ),
              ),
              ListTile(
                  leading: Icon(Icons.photo),
                  title: Text('Profil'),
                  selected: _selectedDestination == 7,
                  onTap: (){
                    selectDestination(7);
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => new PhotoProfil()));
                  }
              ),
              ListTile(
                  leading: Icon(Icons.photo),
                  title: Text('Ajouter une photo de profil'),
                  selected: _selectedDestination == 3,
                  onTap: (){
                    selectDestination(3);
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => new PhotoProfil()));
                  }
              ),
              ListTile(
                leading: Icon(Icons.lock_outline),
                title: Text('Changer le mot de passe'),
                selected: _selectedDestination == 4,
                onTap: () => selectDestination(4),
              ),
              ListTile(
                leading: Icon(Icons.update),
                title: Text('Modifier mes informations'),
                selected: _selectedDestination == 5,
                onTap: () => selectDestination(5),
              ),
              ListTile(
                leading: Icon(Icons.arrow_back),
                title: Text('Déconnexion'),
                selected: _selectedDestination == 6,
                onTap: () {
                  selectDestination(6);
                  logout();
                }
              ),
            ],
          ),
        ),

        body: TabBarView(
          children: [
            Center(
              child: FutureBuilder <List<Cas>>(
              future: futureCas,
              builder: (context, snapshot) {
              if (snapshot.hasData) {
              List<Cas> cas = snapshot.data;
              return ListView.builder(
                  itemCount: cas.length,
                  itemBuilder:(context , index){
                    return Card(
                      margin:EdgeInsets.only(bottom:40.0),
                      child : Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(height: MediaQuery.of(context).size.height/40,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 8.0),
                                width: 60,
                                height: 60,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image:DecorationImage(
                                      image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
                                      fit: BoxFit.fill
                                  ),
                                ),

                              ),
                              Container(
                                margin: EdgeInsets.only(left: 20.0),
                                child:Text('Mamadou Soumah',
                                  style: new TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold
                                  ),
                                ) ,
                              )

                            ],
                          ),
                          Container(
                            padding: EdgeInsets.all(20.0),
                            child: Text(cas[index].description, textAlign: TextAlign.justify,),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.width/1,
                            width: MediaQuery.of(context).size.width/1,
                            child: Carousel(
                              boxFit: BoxFit.cover,
                              autoplay: false,
                              animationCurve: Curves.fastOutSlowIn,
                              animationDuration: Duration(milliseconds: 1000),
                              dotSize: 6.0,
                              dotIncreasedColor: Colors.blue,
                              dotBgColor: Colors.transparent,
                              dotPosition: DotPosition.bottomCenter,
                              dotVerticalPadding: 10.0,
                              showIndicator: true,
                              indicatorBgPadding: 7.0,
                              images: [
                                Image.network('http://amalyy.com/image/imagesCas/${cas[index].image1}',fit: BoxFit.cover),
                                Image.network('http://amalyy.com/image/imagesCas/${cas[index].image2}',fit: BoxFit.cover),
                                Image.network('http://amalyy.com/image/imagesCas/${cas[index].image3}',fit: BoxFit.cover),
                                Image.network('http://amalyy.com/image/imagesCas/${cas[index].image4}',fit: BoxFit.cover),
                              ],
                            ),
                          ),
                          Container(height: 20,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              RaisedButton(
                                  child: Text('500 000 GNF'),
                                  color: Colors.greenAccent,
                                  onPressed: null
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(onSurface: Colors.green),
                                onPressed: () { },

                                child: Text('${cas[index].montant} GNF'),
                              )
                            ],
                          ),
                          SizedBox(
                              width: MediaQuery.of(context).size.width/1.1,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(elevation:15, onSurface: Colors.blue),
                                onPressed: () {
                                  Dialogue("Type de Don");
                                },

                                child: Text('Faire Un Don'),
                              )
                          ),
                          Container(height: 20,)


                        ],
                      ),
                      elevation: 10.0,


                    );

                  }


              );

          } else if (snapshot.hasError) {
          return Text("Veuillez vérifier votre internet");
          }
          // By default show a loading spinner.
          return CircularProgressIndicator();
          },
          ),

      ),

            Center(
                child:new ListView.builder(
                    itemCount: 10,
                    itemBuilder: (context , i){
                      return new Container(
                          padding: EdgeInsets.all(5.0),
                          height: 100.0,
                          child : new Card(
                            color: setColor(),
                            elevation: 5.0,
                            child : new InkWell(
                              onTap: ((){

                                setState(() {
                                  notificationCouleur = false;
                                });

                              }),
                              onLongPress: (()=> print('long')),
                              child: new Container(
                                margin: EdgeInsets.only(left: 10.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: 40,
                                      height: 40,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image:DecorationImage(
                                            image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
                                            fit: BoxFit.fill
                                        ),
                                      ),

                                    ),
                                    Container(
                                        margin: EdgeInsets.only(top:25.0,left: 10.0),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Amina cherif',
                                              style: new TextStyle(
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.bold
                                              ),
                                            ) ,
                                            Text('Description')

                                          ],
                                        )
                                      // margin: EdgeInsets.only(left: 20.0),

                                    )
                                  ],
                                ),
                              ),
                            ),
                            // color: setColor(),
                          )
                      );
                    }
                )

            ),

            Center(
                child: FutureBuilder <List<Structures>>(
                  future: futureStructures,
                  builder: (context, snapshot) {
                  if (snapshot.hasData) {
                  List<Structures> structures = snapshot.data;
                  return ListView.builder(
                  itemCount: structures.length,
                  itemBuilder:(context , index){
                      return new Container(
                        padding: EdgeInsets.all(5.0),
                        height: 100.0,
                        child : new Card(
                          elevation: 5.0,
                          child : new InkWell(
                            onTap: (()=> print('Tape')),
                            onLongPress: (()=> print('long')),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 40,
                                  height: 40,
                                  margin: EdgeInsets.only(left: 15.0),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image:DecorationImage(
                                        image: NetworkImage('http://amalyy.com/image/imagesUsers/${structures[index].photo}'),
                                        fit: BoxFit.fill
                                    ),
                                  ),

                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 10.0),
                                  child: Text(structures[index].compagnie,
                                    style: new TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ) ,
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                  }


                  );

                  } else if (snapshot.hasError) {
                    return Text("Veuillez vérifier votre internet");
                  }
                  // By default show a loading spinner.
                  return CircularProgressIndicator();
                  },
    ),


                )
          ],
        ),
      ),
    );

  }

  Future<Null> Dialogue(String titre ) async
  {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return new SimpleDialog(
          title: new Text(titre, textAlign: TextAlign.center ),
          contentPadding: EdgeInsets.all(10.0),
          children: <Widget>[
            Divider(
              height: 1,
              thickness: 1,
            ),
            SizedBox(height: 20.0),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    new Text('Argent'),
                    new Radio(
                        value: 'Argent',
                        groupValue: strTypeDon,
                        onChanged: (value)
                        {
                          setState(() {
                            strTypeDon = value;
                          });
                        }
                    ),
                  ],
                ),
                new Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    new Text('Biens'),
                    new Radio(
                        value: 'Biens',
                        groupValue: strTypeDon,
                        onChanged: (value)
                        {
                          setState(() {
                            strTypeDon = value;
                          });
                        }
                    ),
                  ],
                ),
              ],
            ),
            new Container(height: 20.0),
            new RaisedButton(
                color: Colors.blue,
                textColor: Colors.white,
                child: new Text('OK'),
                onPressed: (){
                  print ('OK');
                  if(strTypeDon=='Argent'){
                    Navigator.pushReplacement(
                        context, MaterialPageRoute(builder: (context) => new DonArgent()));
                  }else if(strTypeDon=='Biens'){
                    Navigator.pushReplacement(
                        context, MaterialPageRoute(builder: (context) => new DonBien()));
                  }else{
                    Navigator.pop(context);
                  }

                })
          ],
        );

      },
    );
  }

  Color setColor()
  {
    if(notificationCouleur)
    {
      return Colors.blueGrey ;
    }else{
      return Colors.white;
    }
  }


}
